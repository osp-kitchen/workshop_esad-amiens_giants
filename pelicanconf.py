#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'ESAD Amiens'
SITENAME = 'On the shoulders of giants - software tour'
SITEURL = 'Shoulders.giants'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

THEME_STATIC_DIR = 'theme'
THEME = 'theme/giants'
CSS_FILE = 'screen.css'
# STATIC_PATHS = []
# URL settings
ARTICLE_URL = '{category}/{slug}.html'
ARTICLE_SAVE_AS = '{category}/{slug}.html'
PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'
CATEGORY_URL = '{slug}.html'
CATEGORY_SAVE_AS = '{slug}.html'
ARTICLE_LANG_URL = '{category}/{slug}-{lang}.html'
ARTICLE_LANG_SAVE_AS = '{category}/{slug}-{lang}.html'
PAGE_LANG_URL = '{slug}-{lang}.html'
PAGE_LANG_SAVE_AS = '{slug}-{lang}.html'
DRAFT_URL = '{slug}.html'
DRAFT_SAVE_AS = '{slug}.html'

DISPLAY_PAGES_ON_MENU = True
DISPLAY_CATEGORIES_ON_MENU = True

USE_FOLDER_AS_CATEGORY = True

DEFAULT_PAGINATION = 10

PLUGINS = [
    #'compass_process',
    #'minchin.pelican.plugins.image_process',
]

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
