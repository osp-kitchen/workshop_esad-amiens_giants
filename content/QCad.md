Title: QCad
Date: 2018/03/27
Author: Colm
Status: published

## 2d computer aided design, computer aided manufacturing (CAM)

* Who is developing the tool?

Andrew Mustun named `qcad` on Github, owns the development repository https://github.com/qcad and is the main contributor to the project. The repository contributor page lists 13 contributors in total but none of the other 12 seem to have been as active as Andrew Mustun. https://github.com/qcad/qcad/graphs/contributors

* Is the tool under active development?
Yes, currently at version 3

* What is the tool made for?
Designing and manipulating CAD files in 2d. Computer Aided Design is a method of designing objects, assembly plates, parts, with real metric unit precisions. 2d CAD is used for floor plans, cabinets, kitchens, stairs, machine parts,  anything that may need precision plans.

* Who is the tool made for?
(industrical) designers, makers, architects, builders, interior decorators, anybody who has a need for CAD

* Wat is the license of the tool?
GPL3 Gnu Public license 3

* Is it possible to contribute to the tool? How?
Contributions are welcome. The main developer is looking for translations and help with interface buttons. Contributions are asked to be sumbitted through a pull request through github.

* What does the tool cost? Is it a one time payment or a subscription model?
QCad has two 'editions': one Community and one Professional. The differences are as follows:

```
The QCAD Community Edition

The QCAD Community Edition is everything that can be downloaded from our public git repository at github.com/qcad/qcad.

The QCAD Community Edition is distributed under the GPLv3 with exceptions to allow commercial plugins and script extensions.
QCAD Professional

QCAD Professional consists of the QCAD Community Edition with various additional, commercial (proprietary) plugins to provide support for the DWG format, improved support for various DXF format versions, improved support for splines and polyline and various other tools and improvements.
```

* Are there tools alike? What makes this tool different?
Yes, there are many Libre CAD software available, it's a widely needed type of software for engineers and designers alike. This tool seems different because of the spacing and grid presets it has. By default a lot of settings are set to 'industry best pracices' such as milimeter increments, 0,0 origins, and a background grid that desifies by increments of 10 as you zoom.
It also has a command line interaction method that lets you move, copy, mirror or lock objects to different coordinates, itterate over them several times, and run mini scripts.


* Does the tool have a Graphical Interface?
Yes, mostly a GUI tool, but small inclusion of the command line interaction mentionned above.


* Is the interface clear?
It's very dense, but relatively *traditional*. A palet of 'drawing' tools on the left, a canvas in the middle, tool pannels on the right. File and software manipulation in a top bar, an the lovely command box in the bottom.

* Is the interface looking like other applications or is it custom made?
It feels pretty integrated to the rest of my desktop environment. I recognise some pictograms, icons, some window styles etc.

* Does the tool have a command line interface / can you use the tool from the commandline?
No, you can't interact with it from the command line directly, apart from running the app as an executable. However, the development of QCad did enduce the creation of the DXFlib library that parses DXF for manipulation in QCad. DXF stands for Drawing eXchange Format which is a CAD file format developped by Autodesk to allow interoperability between Autocad and other softwares. It's now an open format.

* Can you automate the tool? Does the tool have a scripting interface / API?
A scripting interface no, but still, this command line section for small operations to be performed with precise units rather than mouse moves.

* What are the filetypes the tool can read?
DXF

* What are the filetypes the tool can generate / export?
DXF

* Is there an official documentation and if so where?
https://qcad.org/en/qcad-documentation

* For which operating systems is this tool available?
Windows, Mac and Linux!
