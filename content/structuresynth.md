Title: structuresynth
Date: 2018/03/28
Author: Theo

<!--
available metadata:
Category: mapping (! category is meant to be single)
Tags: maps, command line, vector, dithering, (! tags are multiple)
-->

## 3D Structures generator


* Who is developing the tool?
Mikael Hvidtfeldt Christensen

* Is the tool under active development?
NO

* When did the development of the tool start? / How old is the software?
10 July 2007 ( 11years )

* Does the tool have a Wikipedia page?
No (just reference to the algorithmic art)

* What is the tool made for?
for generating 3D structures by specifying a design grammar.

* Who is the tool made for?
developpers/artists using 3D and programming language

* What is the license of the tool?
It is free software, licensed under the GPL/LPGL license.

* Is it possible to contribute to the tool? How? How many people are contributing?
? (Export template contributions by Subblue, Neon22, Groovelock, SourceZuka, David Bucciarelli, and François Beaune. The Mac builds are provided by David Burnett. Debian and Ubuntu packaging by Miriam Ruiz.)

* What does the tool cost? Is it a one time payment or a subscription model?
This is a free tool

* Are there tools alike? What makes this tool different?
processing, plater, mandelbrot. Simple systems may generate surprising and complex structures

* Does the tool have a Graphical Interface?
Graphical environment with multiple tabs and OpenGL preview

* Is the interface clear?
Yes 

* Is the interface looking like other applications or is it custom made?
Looking like other apps

* Does the tool have a command line interface / can you use the tool from the commandline?
Yes, you use the tool from the commandline

* Can you automate the tool? Does the tool have a scripting interface / API?
?

* What are the filetypes the tool can read?


* What are the filetypes the tool can generate / export?
Native OBJ export

* Is there an official documentation and if so where?
Yes, on the website you can learn some stuffs from shorts tutorials

* For which operating systems is this tool available?
Windows, Linux, Mac OS
